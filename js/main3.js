
import * as THREE from 'https://threejsfundamentals.org/threejs/resources/threejs/r112/build/three.module.js';
import {VRButton} from 'https://threejsfundamentals.org/threejs/resources/threejs/r112/examples/jsm/webxr/VRButton.js';

function main(){
	const canvas = document.querySelector('#c');
	const renderer = new THREE.WebGLRenderer({canvas});
	var allowvr = true;
	const fov = 45;
	const aspect = 2;
	const near = 0.1;
	const far = 100;
	const camera = new THREE.PerspectiveCamera(fov, aspect,near,far);
	camera.position.set(0,10,20);
	
	if (allowvr){
	renderer.xr.enabled = true;
	}else{
	renderer.xr.enabled = false;
	var controls = new THREE.OrbitControls(camera, canvas);
	controls.target.set(0, 0, 0);
	controls.update();
	}
	const scene = new THREE.Scene();


	 //skybox

  	var urlPrefix = "img/sky/";
	var urls = [ urlPrefix + "px.png", urlPrefix + "nx.png",
    urlPrefix + "py.png", urlPrefix + "ny.png",
    urlPrefix + "pz.png", urlPrefix + "nz.png" ];
	
    const loader1 = new THREE.CubeTextureLoader();
    const texture1 = loader1.load(urls);
    scene.background = new THREE.MeshBasicMaterial({color:'#000'});//texture1;
    

	//end skybox

	{
		const sphereGeo = new THREE.SphereBufferGeometry(3,32,32);
		const sphereMat = new THREE.MeshBasicMaterial({color:'#CA8',envMap:texture1});
		const mesh = new THREE.Mesh(sphereGeo, sphereMat);
		mesh.position.set (-4,5,0);
		//scene.add(mesh);
	}


	{
	    const objLoader = new THREE.OBJLoader();
	    objLoader.load('spacecat1.obj', (obj) => {
	    obj.traverse( function( child ) {
	            if ( child instanceof THREE.Mesh ) {
	                //child.material = planeMat;
	               //child.material = new THREE.MeshStandardMaterial();
	               child.material = new THREE.MeshBasicMaterial( { color: 0xffffff, envMap: texture1 } );

	            }
	        } );
	      scene.add(obj);
	    });
  }


	function render(time){
		time *= 0.001;
		controls.update();
		if(resizeRendererToDisplaySize(renderer)){
			const canvas = renderer.domElement;
			camera.aspect = canvas.clientWidth / canvas.clientHeight;
			camera.updateProjectionMatrix();
		}
		
		renderer.render(scene,camera);
		
		requestAnimationFrame(render);
		
	}
		requestAnimationFrame(render);
		
}


function resizeRendererToDisplaySize(renderer){
	const canvas = renderer.domElement;
	const width = canvas.clientWidth;
	const height = canvas.clientHeight;
	const needResize = canvas.width !==width || canvas.height !==height;
	if (needResize){
		renderer.setSize(width,height, false);
	}
	return needResize;
}

main();
