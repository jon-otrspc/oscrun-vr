

<!DOCTYPE>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width,shrink-to-fit=no,user-scalable=no,maximum-scale=1,minimum-scale=1">
    <title>OUTERSPACE</title>
   
    <!--<script src="https://aframe.io/releases/0.9.2/aframe.min.js"></script>-->
    <script src="https://aframe.io/releases/0.9.2/aframe.min.js"></script>
    <script src="https://cdn.rawgit.com/donmccurdy/aframe-extras/v6.0.0/dist/aframe-extras.min.js"></script>
    <script src="https://rawgit.com/fernandojsg/aframe-teleport-controls/master/dist/aframe-teleport-controls.min.js"></script>
    
    <script src="<?php echo get_template_directory_uri(); ?>/js/aframe-particle-system-component.min.js"></script>
   <!-- <script src="https://unpkg.com/aframe-text-geometry-component@0.5.1/dist/aframe-text-geometry-component.min.js"></script>
     <script src="https://unpkg.com/aframe-environment-component@1.1.0/dist/aframe-environment-component.min.js"></script>
   -->
   <script src="https://unpkg.com/aframe-cubemap-component@0.1.4/dist/aframe-cubemap-component.min.js"></script>
   <script src="//cdn.rawgit.com/donmccurdy/aframe-physics-system/v4.0.1/dist/aframe-physics-system.min.js"></script>
   <script src="https://unpkg.com/super-hands@3.0.0/dist/super-hands.min.js"></script>

    <script type="text/javascript">

      // Component to do on click.
      AFRAME.registerComponent('click-listener', {
        init: function () {

            this.el.addEventListener('click', function (evt) {

                // remove clicked object from view
              //  this.setAttribute('visible', false);
                //slower...
                //document.getElementById('cube').setAttribute('visible', false);


            });
        }
      });

      function enterVR(){
        console.log('enterVR');
          let ss = document.getElementById("soundscape");
          //ss.play();
          var entity = document.querySelector('#soundscape');
          entity.components.sound.playSound();
      }

      function exitVR(){
          let ss = document.getElementById("soundscape");
          //ss.play();
          var entity = document.querySelector('#soundscape');
          entity.components.sound.stopSound();
          console.log('exit');
      }

      function aw1(e){
          var e1="sphere"+e;
          var entity = document.getElementById(e1);
          entity.components.sound.playSound();
          entity.setAttribute('color', 'yellow');
      }


      //   Solves Google mute of audio problem (and it is...) https://stackoverflow.com/questions/47921013/play-sound-on-click-in-a-frame?answertab=active#tab-top
      AFRAME.registerComponent('audiohandler', {
        init:function() {
            let playing = false;
            let audio = document.querySelector("#playAudio");
            this.el.addEventListener('click', () => {

                if(!playing) {
                    audio.play();
                } else {
                    audio.pause();
                    audio.currentTime = 0;
                }
                playing = !playing;
            });
        }
      })

      //color randomizer
      AFRAME.registerComponent('color-randomizer', {
        play: function () {
          this.el.addEventListener('drag-drop', function (evt) {
            evt.detail.dropped.setAttribute('material', 'color',
              '#'+(Math.random()*0xFFFFFF<<0).toString(16))
            // color randomizer credit: http://stackoverflow.com/questions/1484506/random-color-generator-in-javascript#comment6801353_5365036
          })
        }
      })
      // turn controller's physics presence on only while button held down
      AFRAME.registerComponent('phase-shift', {
        init: function () {
          var el = this.el
          el.addEventListener('gripdown', function () {
            el.setAttribute('collision-filter', {collisionForces: true})
          })
          el.addEventListener('gripup', function () {
            el.setAttribute('collision-filter', {collisionForces: false})
          })
        }
      })
   

 // animation stuffs   
    var prevData = "clip: idle; crossFadeDuration: .3";;
    var listenerAdded = false;
    var scene = document.querySelector('a-scene');  
    var playOnce = false; 
 
   
    
        
AFRAME.registerComponent('animation-control', {
  schema: {default: ''},
  init() {
     
   /* const bot = document.querySelector('#bun'); 
    var jumpBut = document.querySelector('#jump');
    var runBut = document.querySelector('#run');
    var walkBut = document.querySelector('#walk');
    var idleBut = document.querySelector('#idle');  
    var scene = document.querySelector('a-scene');
    */
   
  
  this.el.addEventListener('click', () => {
        
      if (this.el != jumpBut){  
        bot.setAttribute("animation-mixer",this.data); 
         scene.removeEventListener('animation-loop', jumpTrans);
         prevData = this.data;
         
     }
        
     if (this.el == jumpBut) {
        
        bot.setAttribute("animation-mixer",this.data); 
        var jumpTrans = function () {scene.removeEventListener('animation-loop',jumpTrans); bot.setAttribute("animation-mixer",prevData);console.log("scene ="+scene); console.log("fire")};  
        console.log("prev data from jump button = "+prevData); 
       setTimeout (function(){scene.addEventListener('animation-loop',jumpTrans);},500);     
     
     }
        
     
    }); 
      
      
      
  }
});


  </script>

  </head>
  <body>

    


    <a-scene background="color: #000000">
      <a-entity position="0 2.25 -15" particle-system="preset: dust; particleCount: 300"></a-entity>
      <a-assets>

       
          <a-asset-item id="cat-obj" src="<?php echo get_template_directory_uri(); ?>/3d/cat-body.obj"></a-asset-item>
          <a-asset-item id="cat-paw" src="<?php echo get_template_directory_uri(); ?>/3d/cat-paw.obj"></a-asset-item>
          <img id="os-sign" src="<?php echo get_template_directory_uri(); ?>/img/os-goop.png"></img>
          <img id="skyTexture"
            src="<?php echo get_template_directory_uri(); ?>/img/sky/pinksky.png">
          <img id="contact" src="<?php echo get_template_directory_uri(); ?>/img/contact.png">
          <audio id="relic" src="<?php echo get_template_directory_uri(); ?>/audio/relic1-1.mp3" preload="auto"></audio>
          <audio id="aw1" src="<?php echo get_template_directory_uri(); ?>/audio/aw1.mp3" preload="auto"></audio>
           <audio id="aw2" src="<?php echo get_template_directory_uri(); ?>/audio/aw2.mp3" preload="auto"></audio>
            <audio id="aw3" src="<?php echo get_template_directory_uri(); ?>/audio/aw3.mp3" preload="auto"></audio>
           
      </a-assets>
       <a-entity class="cube" position="1 0.265 -0.5" material="color: green" geometry="" hoverable="" grabbable="" stretchable draggable velocity="" dynamic-body shadow event-set__hoveron="_event: hover-start; material.opacity: 0.2; transparent: true" event-set__hoveroff="_event: hover-end; material.opacity: 1; transparent: false"></a-entity>

      <a-entity cubemap="ext: png; folder: <?php echo get_template_directory_uri(); ?>/img/sky/"></a-entity>
      <!--<a-sky src="#skyTexture"></a-sky>-->
      <!-- Basic movement, selection and teleport  -->
      <a-entity id="rig" movement-controls="constrainToNavMesh: true;" position="0 0 5">
            <a-entity id="cam" camera position="0 1.6 0" look-controls="pointerLockEnabled: true"></a-entity>
                
                <a-entity id="leftController" hand-controls="left" vive-controls="hand: left" oculus-touch-controls="hand: left"
                 windows-motion-controls="hand: left" teleport-controls="cameraRig: #rig; button: trigger; curveShootingSpeed: 20;" visible="true" physics-collider="" phase-shift="" collision-filter="" velocity="" static-body="" super-hands=""></a-entity>

                <a-entity id="rightController" hand-controls="right" vive-controls="hand: right" oculus-touch-controls="hand: right"
                 windows-motion-controls="hand: right" laser-controls raycaster="showLine: true; far: 20; interval: 0; objects: .clickable, a-link;" line="color: lawngreen; opacity: 0.5" visible="true" physics-collider="" phase-shift="" collision-filter="" velocity="" static-body="" super-hands=""></a-entity>
                  <a-entity id="soundscape" sound="src: #relic; autoplay: false; loop: true;"></a-entity>
      
      </a-entity>

      <!-- Normal Hello World objects   -->
      
      <a-sphere dynamic-body hoverable grabbable stretchable draggable droppable id="sphere1" class="clickable" position="4 80 -30" radius="1.25" color="#EF2D5E" sound="src: #aw1; autoplay: false; loop:true" onclick="aw1('1')" click-listener shadow></a-sphere>

      <a-sphere dynamic-body hoverable grabbable stretchable draggable droppable id="sphere2" class="clickable" position="-3 90 -30" radius="1.25" color="#EF2D5E" sound="src: #aw2; autoplay: false; loop:true" onclick="aw1('2')" click-listener shadow></a-sphere>

      <a-sphere dynamic-body hoverable grabbable stretchable draggable droppable id="sphere3" class="clickable" position="14 100 -25" radius="1.25" color="#EF2D5E" sound="src: #aw3; autoplay: false; loop:true" onclick="aw1('3')" click-listener shadow></a-sphere>
     
      <a-plane static-body position="0 -.8 -4" rotation="-90 0 0" width="140" height="140" color="#000" opacity="1" shadow></a-plane>

      <a-entity class="cube" position="1 0.265 -0.5" material="color: green" geometry="" hoverable="" grabbable="" stretchable="" draggable="" event-set__hoveron="" event-set__hoveroff="" velocity="" dynamic-body="" shadow=""></a-entity>

      <!--<a-entity position="0 4.801 -5.127" color="#FFFFFF" text-geometry="value: OUTERSPACE"></a-entity>-->
      <a-entity id="spaceCat" scale="2 2 2" position="0 8 -30">
      <a-obj-model static-body src="#cat-obj"  material="metalness:1; roughness:0; sphericalEnvMap:#skyTexture">
      </a-obj-model>

      <!--cat colliders -->
      <a-sphere static-body radius="2" position="0 2.5 0" color="#EF2D5E" opacity="0"></a-sphere>
      <a-sphere static-body radius="3" position="0 -2 0" color="#EF2D5E" opacity="0"></a-sphere>
      <!--cat colliders end-->
      <a-entity  animation="property: rotation; from: -25 0 0; to: 60 0 0; dir: alternate; loop:true">  
      <a-obj-model src="#cat-paw" position="0 0 0" material="metalness:1; roughness:0; sphericalEnvMap:#skyTexture">
      </a-obj-model>

        

    </a-entity>
  </a-entity>
  <a-entity    id ="bun"
                cursor-listener
                scale="2 2 2"
                position="-3 0 -4"
                rotation="0 0 0"
                animation-mixer="clip: idle; loop: repeat;"
               
                gltf-model="<?php echo get_template_directory_uri(); ?>/3d/bunnywalk3.glb"
       
      ></a-entity> 
   <!-- <a-image src="#os-sign" width="7" height="1" position="0 6 -5.127"></a-image>-->
    </a-scene>
    <div id="ccui" style="position: absolute;left:50px; bottom:50px;"><img
      src="<?php echo get_template_directory_uri(); ?>/img/contact.png" width=290><a href="https://wmurphyrd.github.io/aframe-super-hands-component/examples/physics/">grab test</a></div>
    <script>
      //let vre = document.querySelector("#ccui");
     // vre.addEventListener("click", enterVR);
      document.querySelector('a-scene').addEventListener('enter-vr', function () {
        enterVR();
   console.log("ENTERED VR");
    });
      document.querySelector('a-scene').addEventListener('exit-vr', function () {
        exitVR();
   console.log("EXIT VR");
});
    </script>
  </body>

</html>
